from django import forms
from receipts.models import Receipt, ExpenseCategory, Account


class CreateReceipt(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "date",
            "category",
            "account",
            "tax"
        ]


class CategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ["name"]


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ["name", "number"]
